The Service API module provides a framework for developers to connect with
external services.

It enforces a way of working which will result in decoupled components for
interacting with services that can be reused and interchanged without effecting
each other.

Each service is broken up into three components: a source, parser and processor.
The source is only responsible for fetching the raw data from the service. This
could be making a REST or SOAP call. This response is passed onto the parser.
The parser is responsible for converting the raw response into an intermidate
data structure. For example, decoding a JSON response and storing the data that
is needed. This structure is then passed onto a processor. The processor
operates on the data in the way required. This could be creating a node or
writing data to the database.

To define a service, you need to implement hook_service_api_register_service().

function service_api_example_service_api_register_service() {
  return array(
    // The key is the machine name that will be used to refer to the service.
    'yahoo' => array(
      'description' => 'Connects to yahoo web services.',
      // The handlers for the source and parser are defined here.
      'handlers' => array(
        'source' => 'ExampleRestSource',
        'parser' => 'YahooParser',
      ),
      'endpoints' => array(
        // We can define as many endpoints as we like. Here we only define one.
        'weather' => array(
          // The structure is the intermidiate data transformation that
          // decouples our parser from our processor. It means we can switch out
          // our parser and processor at will as long as we conform to the
          // structure we have provided.
          'structure' => array('title', 'description'),
          'processor' => 'YahooProcessor',
          // A flag that tells the Service API whether we should cache this
          // request or not.
          'cache' => TRUE,
        ),
      ),
    ),
  );
}

The classes defined by in the array can then be implemented. The source class
should extend ServiceAPISource and will automatically look for a function with
the in the format 'endpointnameRequest'. It should return the raw contents of
the service call.

The parser should extend ServiceAPIParser and will automatically look for a
function with the name 'endpointnameParser'. This function receives the response
and needs to convert it into the format that we defined in the structure key.

The processor should extend 'endpointnameProcess'. This recevies the structure
with the data in it and can perform any operation on it.

After these three components have been implemented you can create and send a new
request by doing the following:

// Form a request object for the 'weather' endpoint with the paramaters
// provided.
$request = service_api_create_request('weather', array('location_id' => $wid));
// Send our request to the yahoo service and then display the response on the
// page.
$response = service_api_send('yahoo', $request);

For a full example and documents please see the service_api_example module.
