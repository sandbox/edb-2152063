<?php
/**
 * @file
 * The Source class provides all core processor functionality.
 */

abstract class ServiceAPISource {
  protected $endpoints = array();

  /**
   * Send the request to the service using the dynamically generated method.
   */
  public function sendRequest(ServiceAPIRequest $request) {
    $endpoint = $this->getEndpoint($request);
    $function = $endpoint . "Request";
    $raw = $this->$function($request);
    $response = new ServiceAPIResponse();
    $response->setContent($raw);
    return $response;
  }

  /**
   * Add an endpoint to the list of known endpoints.
   */
  public function addEndpoint($name, $cache) {
    $this->endpoints[] = array('name' => $name, 'cache' => $cache);
  }

  /**
   * Get an endpoint from the list of known endpoints.
   */
  public function getEndpoint(ServiceAPIRequest $request) {
    $name = $request->getEndpoint();
    foreach ($this->endpoints as $endpoint) {
      if ($endpoint['name'] == $name) {
        return $name;
      }
    }
    throw new InvalidArgumentException("Endpoint '$endpoint' has not been defined.");
  }

  /**
   * Determine whether we can cache a particular endpoint.
   */
  public function getEndpointCache(ServiceAPIRequest $request) {
    $name = $request->getEndpoint();
    foreach ($this->endpoints as $endpoint) {
      if ($endpoint['name'] == $name) {
        return $endpoint['cache'];
      }
    }
    throw new InvalidArgumentException("Endpoint '$endpoint' has not been defined.");
  }
}
