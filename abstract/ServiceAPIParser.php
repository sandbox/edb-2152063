<?php
/**
 * @file
 * The parser class provides core functionality for all parsers.
 */

abstract class ServiceAPIParser implements ServiceAPIParserInterface {
  protected $structures = array();

  /**
   * Parse a response into a structured object.
   *
   * @return Structure
   *   Return data in the structure defined.
   */
  public function parse(ServiceAPIRequest $request, ServiceAPIResponse $response) {
    $name = $request->getEndpoint();
    $function = $name . "Parse";
    $structure = $this->getStructure($name);
    $data = $response->getContent();
    return $this->$function($data, $structure);
  }


  /**
   * Get the structure of the data we need to return.
   *
   * @param string $name
   *   The name of the structure we want.
   */
  public function getStructure($name) {
    if (!isset($this->structures[$name])) {
      throw new InvalidArgumentException("Structure '$name' not defined.");
    }

    $structure = $this->structures[$name];
    return new ServiceAPIStructure($structure);
  }

  /**
   * Add a structure.
   */
  public function addStructure($name, $structure) {
    $this->structures[$name] = $structure;
  }
}
