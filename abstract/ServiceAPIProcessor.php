<?php
/**
 * @file
 * The Processor class provides all core processor functionality.
 */

abstract class ServiceAPIProcessor implements ServiceAPIProcessorInterface {
  protected $data;

  /**
   * Data setter.
   */
  public function setData($data) {
    $this->data = $data;
  }

  /**
   * Dynamically generate the process function and call it for an endpoint.
   */
  public function process($name) {
    $function = $name . "Process";
    return $this->$function();
  }
}
