<?php
/**
 * @file
 * The Service API Source interface. All implementing source classes are based off of this.
 */

interface ServiceAPISourceInterface {
  public function sendRequest(ServiceAPIRequest $request);
  public function addEndpoint($endpoint);
  public function getEndpoint(ServiceAPIRequest $request);
}
