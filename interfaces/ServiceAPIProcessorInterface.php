<?php
/**
 * @file
 * The Service API Processor interface.
 */

interface ServiceAPIProcessorInterface {
  public function setData($data);
}
