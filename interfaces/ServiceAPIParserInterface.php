<?php
/**
 * @file
 * The Service API Parser interface.
 */

interface ServiceAPIParserInterface {
  public function parse(ServiceAPIRequest $request, ServiceAPIResponse $response);
  public function addStructure($name, $structure);
  public function getStructure($name);
}
