<?php
/**
 * @file
 * The Request class. An abstraction of any requests that are made to the data 
 * store.
 */

class ServiceAPIRequest {
  private $endpoint;
  private $parameters;

  /**
   * Constructor.
   */
  public function __construct($endpoint, $parameters) {
    $this->setEndpoint($endpoint);
    $this->setParameters($parameters);
  }

  /**
   * Get the endpoint we are querying.
   */
  public function getEndpoint() {
    return $this->endpoint;
  }

  /**
   * Get the parameters.
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Set the endpoint.
   */
  public function setEndpoint($endpoint) {
    $this->endpoint = $endpoint;
  }

  /**
   * Set the parameters.
   */
  public function setParameters($parameters) {
    $this->parameters = $parameters;
  }

  /**
   * Generate a unique caching id for this request.
   */
  public function getCacheId() {
    return $this->endpoint . ":" . md5(serialize($this->parameters));
  }

}
