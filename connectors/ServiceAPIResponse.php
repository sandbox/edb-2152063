<?php
/**
 * @file
 * The Response class. An abstraction of any response we receive from the 
 * service.
 */

class ServiceAPIResponse {
  private $content;

  /**
   * Set the content.
   */
  public function setContent($content) {
    $this->content = $content;
  }

  /**
   * Get the content.
   */
  public function getContent() {
    return $this->content;
  }
}
