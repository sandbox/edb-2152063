<?php
/**
 * @file
 * The Response class. An abstraction of any response we receive from the 
 * service.
 */

class ServiceAPIStructure {
  private $data;

  /**
   * Constructor.
   *
   * @param array $map
   *   A list of elements that are available to use on the structure.
   */
  public function __construct($map) {
    foreach ($map as $item) {
      $this->data[$item] = NULL;
    }
  }

  /**
   * Magic get method.
   *
   * Ensure we are only getting items defined by our structure.
   */
  public function __get($name) {
    if (!isset($this->data[$name])) {
      throw new InvalidArgumentException("Getting. '$name' has not been defined in the data map.");
    }
    return $this->data[$name];
  }

  /**
   * Magic set method.
   * 
   * Ensure we are only setting items defined by our structure.
   */
  public function __set($name, $value) {
    if (isset($this->data[$name])) {
      throw new InvalidArgumentException("Setting. '$name' has not been defined in the data map.");
    }
    $this->data[$name] = $value;
  }
}
