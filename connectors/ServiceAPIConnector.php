<?php
/**
 * @file
 * The Service API Connector. Does all the heavy listing between the source, 
 * parser and processor comopnents.
 */

class ServiceAPIConnector {
  private $source;
  private $parser;
  private $processors = array();

  /**
   * Send a request to the endpoint.
   * 
   * Pass it through the parser and onto the processor for the particular
   * endpoint.
   *
   * @param ServiceAPIRequest $request
   *   The request to execute.
   */
  public function sendRequest(ServiceAPIRequest $request) {
    $cid = $request->getCacheId();
    $endpoint = $request->getEndpoint();
    $response = $this->source->sendRequest($request);
    $data = $this->parser->parse($request, $response);
    $processor = $this->getProcessor($endpoint);
    $processor->setData($data);
    return $processor->process($endpoint);
  }

  /**
   * Check whether the connector object has been successfully set up.
   */
  public function isSetUp() {
    return $this->source && $this->parser && $this->processors;
  }

  /**
   * Source setter.
   */
  public function setSource($source) {
    $this->source = $source;
  }

  /**
   * Source getter.
   */
  public function getSource() {
    return $this->source;
  }

  /**
   * Parser setter.
   */
  public function setParser($parser) {
    $this->parser = $parser;
  }

  /**
   * Add a processor for a particular endpoint.
   *
   * @param string $name
   *   The name of the endpoint we want to add the processor to.
   * @param ServiceAPIProcessor $processor
   *   The processor.
   */
  public function addProcessor($name, $processor) {
    $this->processors[$name] = $processor;
  }

  /**
   * Get the processor for a particular endpoint.
   *
   * @param string $name
   *   The name of the endpoint.
   *
   * @return ServiceAPIProcessor
   *   The processor.
   */
  public function getProcessor($name) {
    if (!isset($this->processors[$name])) {
      throw new InvalidArgumentException("Processor '$name' not defined.");
    }

    return $this->processors[$name];
  }
}
